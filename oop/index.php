<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new Animal("shaun");

echo "Name :  $object->name <br>";
echo "Legs : $object->legs <br>";
echo "Cold blooded : $object->cold_blooded <br><br>";

$kodok = new Frog("buduk");

echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold blooded : $kodok->cold_blooded";
$kodok->buduk();

$sungokong = new Ape("kera sakti");

echo "<br><br>Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold blooded : $sungokong->cold_blooded<br>";
$sungokong->kerasakti();