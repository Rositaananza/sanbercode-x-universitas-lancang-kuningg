<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Jawaban soal no 1</h3>";

        echo "<h4> a </h4>";
            $first_sentence = "Hello PHP!";

            echo "Kalimat soal 1 : $first_sentence <br>";
            echo "Panjang Kalimat soal 1 : " . strlen($first_sentence)."<br>";
            echo "Jumlah Kata soal 1 : " . str_word_count($first_sentence) . "<br>";

         echo "<h4> b </h4>";
            $second_sentence = "I'm ready for the challenges";

            echo "Kalimat soal 2 : $second_sentence <br>";
            echo "Kalimat soal 2 : " . strlen($second_sentence). "<br>";
            echo "Panjang Kata soal 2 : ".str_word_count($second_sentence). "<br>";


        echo "<h3> Jawaban soal no 2 </h3>";
    
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: " . substr($string2,1,5) . "<br>";
        echo "Kata Ketiga: " . substr($string2, 6, 8) . "<br>";

        echo "<h3> Jawaban soal no 3 </h3>";
       
        $string3 = "PHP is old but sexy!";
        echo "Soal 3 : $string3 <br>";
        echo "Soal 3 ganti string : " . str_replace("sexy", "awesome", $string3);

    ?>
</body>
</html>