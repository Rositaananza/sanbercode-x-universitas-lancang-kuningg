<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3>Jawaban Soal No 1 Looping I Love PHP Ascending</h3>";
        $a=2;
        do {
            echo "$a - Angka Genap <br>";
            $a+=2;
        } while ($a <=20);

     
        echo "<h3>Jawaban Soal No 1 Looping I Love PHP Descending</h3>";
        
        
        for ($i =20; $i>=1; $i-=2){
            echo "$i - Angka Genap <br>";
        }


        echo "<h3>Jawaban Soal No 2 </h3>";

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);

        foreach ($numbers as $nilai){
            $tampung[] = $nilai %5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah: ";
        print_r($tampung);
        echo "<br>";


        echo "<h3>Jawaban Soal No 3 </h3>";

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach ($items as $item){
        $output = [
            "id" => $item[0],
            "name" => $item[1],
            "price" => $item[2],
            "descrption" => $item[3],
            "source" => $item[4],
            
        ]; 
        print_r($output);
        echo "<br>";
        };

       
        echo "<h3>Jawaban Soal No 4</h3>";

    echo "Asterix: ";
    echo "<br>";  

    for ($i = 1; $i <= 5; $i++) {
        for ($j = 1; $j <= $i; $j++) {
            echo "* ";
        }
        echo "<br>";
    }
?>

</body>
</html>