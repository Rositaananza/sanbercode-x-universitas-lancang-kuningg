<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Jawaban soal no 1 kids </h3>";
    
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    print_r($kids);
    echo "<ul>";
    echo "<li>" . $kids[0] . "</kids>";
    echo "<li>" . $kids[1] . "</kids>";
    echo "<li>" . $kids[2] . "</kids>";
    echo "<li>" . $kids[3] . "</kids>";
    echo "<li>" . $kids[4] . "</kids>";
    echo "<li>" . $kids[5] . "</kids>";
    echo "<?ul>";


    echo "<h3> Jawaban soal no 1 adults </h3>";
    
    $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
    print_r($adults);

    echo "<ul>";
    echo "<li>" . $adults[0] . "</adults>";
    echo "<li>" . $adults[1] . "</adults>";
    echo "<li>" . $adults[2] . "</adults>";
    echo "<li>" . $adults[3] . "</adults>";
    echo "<li>" . $adults[4] . "</adults>";
    echo "<?ul>";


    echo "<h3> Jawaban soal no 2 </h3>";
    
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: " . count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    foreach ($kids as $kid) {
        echo "<li>$kid</li>";
    }
    echo "</ol>";

    echo "Total Adults: " . count($adults); 
    echo "<br>";
    echo "<ol>";
    foreach ($adults as $adult) {
        echo "<li>$adult</li>";
    }
    echo "</ol>";

    echo "<h3> Jawaban soal no 3 </h3>";
$characters = [
    [
        "Name" => "Will Byers",
        "Age" => 12,
        "Aliases" => "Will the Wise",
        "Status" => "Alive"
    ],
    [
        "Name" => "Mike Wheeler",
        "Age" => 12,
        "Aliases" => "Dungeon Master",
        "Status" => "Alive"
    ],
    [
        "Name" => "Jim Hopper",
        "Age" => 43,
        "Aliases" => "Chief Hopper",
        "Status" => "Deceased"
    ],
    [
        "Name" => "Eleven",
        "Age" => 12,
        "Aliases" => "El",
        "Status" => "Alive"
    ]
];

echo "<pre>";
print_r($characters);
echo "</pre>";
?>
</body>
</html>
